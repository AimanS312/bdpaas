import java.io.*;
import java.util.*;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.fs.FileSystem;

public class FastSimilarity {

	public static Float threshold = 0.8f;

  public static class FastMapper
       extends Mapper<LongWritable, Text, Text, Text> {

		private Text outputKey = new Text();

    public void map(LongWritable key, Text value, Context context
                    ) throws IOException, InterruptedException {

			String[] words = value.toString().split("\t")[1].split(" ");

			int pCount = ((Double) (words.length - Math.ceil(threshold * words.length) + 1)).intValue();

			for (int i = 0; i < pCount; i += 1) {
				outputKey.set(words[i]);
				context.write(outputKey, value);
			}
    }
  }

  public static class FastReducer
       extends Reducer<Text,Text,Text,FloatWritable> {

		static enum CountersEnum { NCOMPARISONS }

		private Text outputKey = new Text();
		private FloatWritable outputValue = new FloatWritable();

    public void reduce(Text key, Iterable<Text> values, Context context
                       ) throws IOException, InterruptedException {

			List<Long> docIds = new ArrayList<Long>();
			List<String> docs = new ArrayList<String>();

			for (Text val : values) {
				docIds.add(Long.parseLong(val.toString().split("\t")[0]));
				docs.add(val.toString().split("\t")[1]);
			}

			for (int i = 0; i < docIds.size(); i += 1) {
				Long docId1 = docIds.get(i);
				String doc1 = docs.get(i);

				for (int j = i + 1; j < docIds.size(); j += 1) {
					Long docId2 = docIds.get(j);
					String doc2 = docs.get(j);

					Float similarity = computeSimilarity(doc1, doc2);

					Counter counter = context.getCounter(CountersEnum.class.getName(),
						CountersEnum.NCOMPARISONS.toString());
					counter.increment(1);

				  if (similarity > threshold) {
						String idCouple = (docId1 < docId2) ? Long.toString(docId1) + "$" + Long.toString(docId2) : Long.toString(docId2) + "$" + Long.toString(docId1);
						outputKey.set(idCouple);
						outputValue.set(similarity);
					  context.write(outputKey, outputValue);
				  }
				}
			}
    }

		public static Float computeSimilarity(String doc1, String doc2) {

			Float similarity = 0.8f;
			Set<String> doc1Set = new HashSet<String>(Arrays.asList(doc1.split(" ")));
			Set<String> doc2Set = new HashSet<String>(Arrays.asList(doc2.split(" ")));
			Set<String> union = new HashSet<String>(doc1Set);
			Set<String> intersection = new HashSet<String>(doc2Set);
			union.addAll(doc2Set);
			intersection.retainAll(doc1Set);

			return (new Float(intersection.size())) / (new Float(union.size()));
		}
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    FileSystem fs = FileSystem.get(conf);

    if (fs.exists(new Path(args[1]))) {
      fs.delete(new Path(args[1]), true);
    }

    conf.set("mapred.TextOutputFormat.separator", ",");

    Job job = new Job(conf, "set similarity joins prefix");
    job.setJarByClass(FastSimilarity.class);
    job.setReducerClass(FastReducer.class);
    job.setMapperClass(FastMapper.class);
    job.setMapOutputKeyClass(Text.class);
    job.setMapOutputValueClass(Text.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(FloatWritable.class);

    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));

    long tic = System.currentTimeMillis();
    if (job.waitForCompletion(true)) {
      long toc = System.currentTimeMillis();
      double duration = (toc - tic) / 1000.0;
      System.out.println("Eclapsed time: " + duration + " s \n");
      System.exit(0);
    } else {
      System.exit(1);
    }
  }
}
