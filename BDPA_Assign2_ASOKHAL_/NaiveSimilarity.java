import java.io.*;
import java.util.*;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.fs.FileSystem;

public class NaiveSimilarity {

  public static class NaiveMapper
       extends Mapper<LongWritable, Text, Text, Text>{

		private Text finalKey = new Text();
		private Text finalValue = new Text();

		public Long mID = 1000L;

    public void map(LongWritable key, Text value, Context context
                    ) throws IOException, InterruptedException {
			Long doc = Long.parseLong(value.toString().split("\t")[0]);
			finalValue.set(value.toString().split("\t")[1]);

			for (Long id = 1L; id < doc; id = id + 1L) {
				finalKey.set(Long.toString(id) + "$" + Long.toString(doc));
				context.write(finalKey, finalValue);
			}

			for (Long id = doc + 1L; id < mID + 1L; id = id + 1L) {
				finalKey.set(Long.toString(doc) + "$" + Long.toString(id));
				context.write(finalKey, finalValue);
			}
    }
  }

  public static class NaiveReducer
       extends Reducer<Text,Text,Text,FloatWritable> {

		static enum CounterE { NCOMPARISONS }

		public static Float threshold = 0.8f;
		private FloatWritable finalValue = new FloatWritable();

    public void reduce(Text key, Iterable<Text> values, Context context
                       ) throws IOException, InterruptedException {

			String doc1 = values.iterator().next().toString();
		  String doc2 = values.iterator().next().toString();

			Counter counter = context.getCounter(CounterE.class.getName(),
				CounterE.NCOMPARISONS.toString());
			counter.increment(1);

		  Float similarity = computeSimilarity(doc1, doc2);

		  if (similarity > threshold) {
				finalValue.set(similarity);
			  context.write(key, finalValue);
		  }
    }

		public static Float computeSimilarity(String doc1, String doc2) {

			Float similarity = 0.8f;
			Set<String> doc1Set = new HashSet<String>(Arrays.asList(doc1.split(" ")));
			Set<String> doc2Set = new HashSet<String>(Arrays.asList(doc2.split(" ")));
			Set<String> union = new HashSet<String>(doc1Set);
			Set<String> intersection = new HashSet<String>(doc2Set);
			union.addAll(doc2Set);
			intersection.retainAll(doc1Set);

			return (new Float(intersection.size())) / (new Float(union.size()));
		}
  }


  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    FileSystem fs = FileSystem.get(conf);

    if (fs.exists(new Path(args[1]))) {
      fs.delete(new Path(args[1]), true);
    }

    conf.set("mapred.TextOutputFormat.separator", ",");

    Job job = new Job(conf, "set similarity joins naive");
    job.setJarByClass(NativeSimilarity.class);
    job.setReducerClass(NaiveReducer.class);
    job.setMapperClass(NaiveMapper.class);
    job.setMapOutputKeyClass(Text.class);
    job.setMapOutputValueClass(Text.class);
    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(FloatWritable.class);

    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));

    long tic = System.currentTimeMillis();
    if (job.waitForCompletion(true)) {
      long toc = System.currentTimeMillis();
      double duration = (toc - tic) / 1000.0;
      System.out.println("Eclapsed time: " + duration + " s \n");
      System.exit(0);
    } else {
      System.exit(1);
    }
  }
}
