import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import java.util.HashMap;
import java.util.HashSet;
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.Text;

public class Preprocess {
  public static HashMap<String, Integer> readMyCSV(){
    HashMap<String, Integer> myCSV = new HashMap<String, Integer>();
    BufferedReader buffredReader = null;
    String line = "";
    String inputFile = "WordCount.csv";

    try {
      buffredReader = new BufferedReader(new FileReader(inputFile));
      while ((line = buffredReader.readLine()) != null) {
        String[] sLine = line.split(",");
        myCSV.put(sLine[0], Integer.parseInt(sLine[1].trim()));
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (buffredReader != null) {
        try {
          buffredReader.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return myCSV;
    }

    public static class Cleaner extends Mapper<LongWritable, Text, LongWritable, Text>{
      private Text line = new Text();
      private HashMap<String, Integer> wc = Preprocess.readMyCSV();
      private class OccComparator implements Comparator<String> {
        @Override
        public int compare(String wd1, String wd2){
          return wc.get(wd1).compareTo(wc.get(wd2));
        }
      }

      public void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] words = value.toString().toLowerCase().split("[^a-zA-Z0-9]");
        HashSet<String> ws = new HashSet<String>();
        for (String word : words) {
          if (wc.get(word) < 4000){
            ws.add(word);
          }
        }
        String[] uw = ws.toArray(new String[ws.size()]);
        Arrays.sort(uw, new OccComparator());
        String cLine = new String();
        for (int i=0; i<uw.length; i++){
          cLine += uw + " ";
        }
        cLine = cLine.trim();
        if (cLine.length() > 0) {
          line.set(cLine);
          context.write(key, line);
        }
      }
    }

    public static void main(String[] args) throws Exception {
      Configuration conf = new Configuration();
      FileSystem fs = FileSystem.get(conf);


      if (fs.exists(new Path(args[1]))) {
        fs.delete(new Path(args[1]), true);
      }


      conf.set("mapred.TextOutputFormat.separator", ",");

      Job job = new Job(conf, "Preprocess");
      job.setJarByClass(Preprocess.class);
      job.setNumReduceTasks(0);
      job.setMapperClass(Cleaner.class);

      job.setOutputKeyClass(LongWritable.class);
      job.setOutputValueClass(Text.class);
      FileInputFormat.addInputPath(job, new Path(args[0]));
      FileOutputFormat.setOutputPath(job, new Path(args[1]));


      long tic = System.currentTimeMillis();
      if (job.waitForCompletion(true)) {
        long toc = System.currentTimeMillis();
        double duration = (toc - tic) / 1000.0;
        System.out.println("Eclapsed time: " + duration + " s \n");
        System.exit(0);
      } else {
        System.exit(1);
      }
    }
  }
