import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Collections;

import java.util.HashMap;
import java.util.HashSet;
import java.io.IOException;
import java.util.StringTokenizer;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.io.MapWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.Text;

public class InvertedIndex {
  public static enum Counter {WORDS};

  public static HashSet<String> readWordsInCSV() {
    HashSet words = new HashSet<String>();

    String inputPathCSV = "stopWord.csv";
    BufferedReader br = null;
    String line = "";

    try {
      br = new BufferedReader(new FileReader(inputPathCSV));
      while ((line = br.readLine()) != null) {
        String[] strings = line.split(",");
        words.add(strings[0]);
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } finally {
      if (br != null) {
        try {
          br.close();
        } catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
    return words;
  }

  public static class TokenizerMapper extends Mapper<Object, Text, Text, Text> {
    private Text word = new Text();
    private Text file = new Text();
    private HashSet<String> stopWord = InvertedIndex.readWordsInCSV();
    private IntWritable one = new IntWritable(1);
    private MapWritable list = new MapWritable();



    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
      //get input file name in the mapper
      FileSplit fsplit = (FileSplit) context.getInputSplit();
      String inputFileName = fsplit.getPath().getName();
      file.set(inputFileName);
      StringTokenizer tokenizer = new StringTokenizer(value, " .,;:!?<>/(){}[]-_|*$#'\"");
      String token = new String();
      while (tokenizer.hasMoreTokens()) {
        token = word.set(tokenizer.nextToken().toLowerCase());
        // Set the word if it isn't in stopWord
        if (!stopWord.contains(token)) {
          word.set(token);
          list.clear();
          list.put(file, one);
          //StringBuilder builder = new StringBuilder();
          //builder.append(file + "#" + one.toString());

          context.write(word, list);
        }
      }
    }
  }

  public static class TokenCombiner extends Reducer<Text, MapWritable, Text, MapWritable> {
    private MapWritable list = new MapWritable();

    public void reduce(Text word, Iterable<MapWritable> mws, Context context) throws IOException, InterruptedException {
      list.clear();
      for (MapWritable mw : mws) {
        Text file = entry.getKey();
        IntWritable nV = (IntWritable) entry.getValue();
        if (list.containsKey(file)) {
          IntWritable oV = (IntWritable) list.get(file);
          IntWritable fV = new IntWritable(oV.get() + nV.get());
        } else {
          IntWritable fV = new IntWritable(nV.get());
        }
          list.put(file, fV);
        }
        context.write(word, list);
      }
    }

  public static class TokenReducer extends Reducer<Text, MapWritable, Text, MapWritable> {
    private MapWritable list = new MapWritable();
    protected boolean isLastWord = false;

    public void reduce(Text word, Iterable<MapWritable> mws, Context context) throws IOException, InterruptedException {
      list.clear();
      for (MapWritable mw : mws) {
        Text file = entry.getKey();
        IntWritable nV = (IntWritable) entry.getValue();
        if (list.containsKey(file)) {
          IntWritable oV = (IntWritable) list.get(file);
          IntWritable fV = new IntWritable(oV.get() + nV.get());
        } else {
          IntWritable fV = new IntWritable(nV.get());
        }
          list.put(file, fV);
      }
      isLastWord = (list.size()==1);
      if (isLastWord) {
        context.getCounter(Counter.WORDS).increment(1);
      }
      context.write(word, list);
    }
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    FileSystem fs = FileSystem.get(conf);

    if (fs.exists(new Path(args[1]))) {
      fs.delete(new Path(args[1]), true);
    }
    //Compression: http://stackoverflow.com/questions/5571156/hadoop-how-to-compress-mapper-output-but-not-the-reducer-output
    if (Integer.parseInt(args[2]) == 1) {
      conf.set("mapreduce.map.output.compress", "true");
    }
    // write as csv: http://stackoverflow.com/questions/16329884/how-can-i-output-hadoop-result-in-csv-format
    conf.set("mapred.TextOutputFormat.separator", ",");

    Job job = new Job(conf, "Inverted Index");
    job.setJarByClass(InvertedIndex.class);

    // Set number of reducers
    job.setNumReduceTasks(Integer.parseInt(args[3]));
    // Set Combiner
    if (Integer.parseInt(args[4]) == 1) {
      job.setCombinerClass(TokenCombiner.class);
    }

    job.setMapperClass(TokenizerMapper.class);
    job.setReducerClass(TokenReduce.class);

    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(MapWritable.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    long startTime = System.nanoTime();
    if (job.waitForCompletion(true)) {
      Counters counters = job.getCounters();
      Counter uniqueWord = counters.findCounter(TaskCounter.REDUCE_INPUT_GROUPS);
      Counter assemblor = counters.findCounter(WordCounter.WORDS);

      // write the count in HDFS
      Path path = new Path("words.csv");
      if (hdfs.exists(path)) {
          hdfs.delete(path, true);
      }
      FSDataOutputStream out = hdfs.create(path);
      out.writeUTF(message);
      out.close();

      System.exit(0);
    }
    else {
      System.exit(1);
    }
  }
}
