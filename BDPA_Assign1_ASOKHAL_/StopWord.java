import java.io.IOException;
import java.util.*;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;

public class StopWord {

  public static class TokenizerMapper extends Mapper<Object, Text, Text, IntWritable> {
    private final static IntWritable one = new IntWritable(1);
    private Text word = new Text();

    public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
      String line = value.toString();
      StringTokenizer tokenizer = new StringTokenizer(line, " .,;:!?<>/(){}[]-_|*$#'\"");
      while (tokenizer.hasMoreTokens()) {
        word.set(tokenizer.nextToken().toLowerCase());
        context.write(word, one);
      }
    }
  }

  public static class TokenReduce extends Reducer<Text, IntWritable, Text, IntWritable> {

    public void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
      int sum = 0;
      for (IntWritable val : values) {
        sum += val.get();
      }
      if (sum > 4000) {
        context.write(key, new IntWritable(sum));
      }
    }
  }

  public static void main(String[] args) throws Exception {
    Configuration conf = new Configuration();
    FileSystem fs = FileSystem.get(conf);

		if (fs.exists(new Path(args[1]))) {
			fs.delete(new Path(args[1]), true);
		}
    //Compression: http://stackoverflow.com/questions/5571156/hadoop-how-to-compress-mapper-output-but-not-the-reducer-output
    if (Integer.parseInt(args[2]) == 1) {
      conf.set("mapreduce.map.output.compress", "true");
    }
    // write as csv: http://stackoverflow.com/questions/16329884/how-can-i-output-hadoop-result-in-csv-format
    conf.set("mapred.TextOutputFormat.separator", ",");

    Job job = new Job(conf, "StopWord");
    job.setJarByClass(StopWord.class);

    // Set number of reducers
    job.setNumReduceTasks(Integer.parseInt(args[3]));
    // Set Combiner
    if (Integer.parseInt(args[4]) == 1) {
      job.setCombinerClass(TokenReduce.class);
    }

    job.setMapperClass(TokenizerMapper.class);
    job.setReducerClass(TokenReduce.class);

    job.setOutputKeyClass(Text.class);
    job.setOutputValueClass(IntWritable.class);
    FileInputFormat.addInputPath(job, new Path(args[0]));
    FileOutputFormat.setOutputPath(job, new Path(args[1]));
    long tic = System.nanoTime();
    if (job.waitForCompletion(true)) {
      long toc = System.nanoTime();
      float duration = (toc - tic);
      duration /= 100000000;
      System.out.println(" Elapsed: " + duration + "s \n");
      System.exit(0);
    } else {
      System.exit(1);
    }
  }
}
